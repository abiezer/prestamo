<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ page import="modelo.DAO.RegionDAO" %>
    <%@ page import="modelo.Region" %>
    <%@ page import="modelo.DAO.ComunaDAO" %>
    <%@ page import="modelo.Comuna" %>
    <%@ page import="java.util.ArrayList" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" href="css/bootstrap.css" crossorigin="anonymous">

<title>Registrate</title>

	<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
	<script>
	/*
	$(document).ready(function(){
		$("#region_id").change(function(){
			var region_id = $("#region_id").val()
			alert(region_id);
			var url = "comuna";
			$.get(url,{region_id:region_id},function(data){
				$( ".result" ).html( data );
				
			});
		});
	});
	*/
	
	$(document).ready(function(){
		$("#region_id").change(function(){
			var region_id = $("#region_id").val();  
			//alert("el valor de la variable es: " + region_id);
			
			$.get("ComunaCarga",{region_id:region_id},function(data){
				$(".result").html(data);
			});
			
		});
	});
	
	</script>
	
		
</head>
<body>
<select>
	<option value="1">Los RIos</option>
	<option value="2">Los RIos</option>
	<option value="3">Los RIos</option>
	<option value="4">Los RIos</option>
</select>





	<h1>Registrate</h1>
	<form action="usuario" method="POST">
		<label>Rut</label><br>
		<input type="text" name="rut"/>
		
		<br><label>Nombre</label><br>
		<input type="text" name="nombre"/>
		
		<br><label>Apellido</label><br>
		<input type="text" name="apellido"/>
		
		<br><label>Correo</label><br>
		<input type="text" name="correo"/>
		
		<br><label>Tipo</label><br>
		<input type="text" name="tipo"/>
		
		<br><label>Calle</label><br>
		<input type="text" name="calle"/>
		
		<br><label>Casa</label><br>
		<input type="text" name="numeroCasa"/>
		
		<br><label>Region</label><br>
		<select name="region_id" id="region_id">
			<%
			RegionDAO regionDAO = new RegionDAO();
			ArrayList<Region> regiones = regionDAO.get(100);
			
			for(Region region: regiones){
				
				out.print("<option value='"+region.getId()+"'>");
				out.print(region.getNombre());		
				out.print("</option>");
			}
			%>
		</select>
		
		
		
		<button type="submit">Guardar</button>
	</form>
	<div class="result"></div>	
</body>
</html>