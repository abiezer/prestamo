<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<html>
	<head>
		<meta charset="ISO-8859-1">
		<title>Region</title>
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
	</head>
	<body>
	<jsp:include page="Menu.jsp" />
	<h1>Registre una Region</h1>
	<form action="region" method="GET">
	<div class="form-group">
		<label for="nombre" >Nombre</label>
		<br>
		<input class="form-control" type="text" name="nombre"/>
		<br>
		<button class="btn btn-primary" type="submit">Guardar</button>
	</div>	
	</form>

	</body>

</html>