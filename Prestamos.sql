/****** Script para Plataforma de Prestamos ******/

CREATE TABLE Region(
	id INT IDENTITY PRIMARY KEY,
	nombre VARCHAR(255)
)

CREATE TABLE Comuna(
	id INT IDENTITY PRIMARY KEY,
	nombre VARCHAR(255),
	region_id INT FOREIGN KEY REFERENCES Region(id) ON DELETE SET NULL ON UPDATE CASCADE
)

CREATE TABLE Reputacion(
	id INT IDENTITY PRIMARY KEY,
	clasificacion VARCHAR(50),
	procentaje float
)

CREATE TABLE Usuario(
	id INT IDENTITY PRIMARY KEY,
	rut VARCHAR(50),
	nombre VARCHAR(100),
	apellido VARCHAR(100),
	correo VARCHAR(100) CONSTRAINT correoUnico UNIQUE,
	tipo VARCHAR(50),
	calle VARCHAR(100),
	numerocasa VARCHAR(100),
	reputacion_id INT FOREIGN KEY REFERENCES Reputacion(id) ON DELETE SET NULL ON UPDATE CASCADE,
	comuna_id INT FOREIGN KEY REFERENCES Comuna(id) ON DELETE SET NULL ON UPDATE CASCADE,
)

CREATE TABLE Deposito(
	id INT IDENTITY PRIMARY KEY,
	tipo VARCHAR(50),
	fecha DATE,
	monto float,
	usuario_id INT FOREIGN KEY REFERENCES Usuario(id) ON DELETE SET NULL ON UPDATE CASCADE,
)

CREATE TABLE Prestamo(
	id INT IDENTITY PRIMARY KEY,
	fecha DATE,
	motivo varchar(1000),
	monto float,
	cuotas int,
	usuario_id INT FOREIGN KEY REFERENCES Usuario(id) ON DELETE SET NULL ON UPDATE CASCADE,
)

CREATE TABLE Movimiento(
	id INT IDENTITY PRIMARY KEY,
	tipo VARCHAR(100),
	monto FLOAT,
	usuario_id INT FOREIGN KEY REFERENCES Usuario(id) ON DELETE SET NULL ON UPDATE CASCADE,
	prestamo_id INT FOREIGN KEY REFERENCES Prestamo(id) ON DELETE SET NULL ON UPDATE CASCADE
)

CREATE TABLE Deposito_Movimiento(
	id INT IDENTITY PRIMARY KEY,
	deposito_id INT FOREIGN KEY REFERENCES Deposito(id) ON DELETE SET NULL ON UPDATE CASCADE,
	prestamo_id INT FOREIGN KEY REFERENCES Prestamo(id) ON DELETE SET NULL ON UPDATE CASCADE
)

SELECT * FROM Region
SELECT * FROM Usuario
SELECT * FROM Deposito

INSERT INTO Reputacion VALUES('AAA',5)
INSERT INTO Reputacion VALUES('BBB',6)
INSERT INTO Reputacion VALUES('CCC',7)
INSERT INTO Reputacion VALUES('DDD',8)
INSERT INTO Reputacion VALUES('FFF',9)

INSERT INTO Region VALUES('I de Tarapacá')
INSERT INTO Region VALUES('II de Antofagasta')
INSERT INTO Region VALUES('III de Atacama')
INSERT INTO Region VALUES('IV de Coquimbo')
INSERT INTO Region VALUES('V de Valparaíso')
INSERT INTO Region VALUES('VI del Libertador General Bernardo O’Higgins')
INSERT INTO Region VALUES('VII del Maule')
INSERT INTO Region VALUES('VIII de Concepción')
INSERT INTO Region VALUES('IX de la Araucanía')
INSERT INTO Region VALUES('X de Los Lagos')
INSERT INTO Region VALUES('XI de Aysén del General Carlos Ibañez del Campo')
INSERT INTO Region VALUES('XII de Magallanes y de la Antártica Chilena')
INSERT INTO Region VALUES('Metropolitana de Santiago')
INSERT INTO Region VALUES('XIV de Los Rios')
INSERT INTO Region VALUES('XV de Arica y Parinacota')
INSERT INTO Region VALUES('XVI del Ñuble')

INSERT INTO Comuna VALUES('Valdivia',16)
INSERT INTO Usuario VALUES('26.361.396-1','Abiezer','Sifontes','abiezersifontes@gmail.com','P','Bouchef','867',5,1)
INSERT INTO Deposito VALUES
