package modelo;

public class Usuario {
	private int id;
	private String rut;
	private String nombre;
	private String apellido;	
	private String correo;
	private String tipo;
	private String calle;
	private String numeroCasa;
	private Integer reputacionId=null;
	private Integer comunaId=null;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getRut() {
		return rut;
	}
	public void setRut(String rut) {
		this.rut = rut;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}	
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public String getCalle() {
		return calle;
	}
	public void setCalle(String calle) {
		this.calle = calle;
	}
	public String getNumeroCasa() {
		return numeroCasa;
	}
	public void setNumeroCasa(String numeroCasa) {
		this.numeroCasa = numeroCasa;
	}
	public int getReputacionId() {
		return reputacionId;
	}
	public void setReputacionId(int reputacionId) {
		this.reputacionId = reputacionId;
	}
	public int getComunaId() {
		return comunaId;
	}
	public void setComunaId(int comunaId) {
		this.comunaId = comunaId;
	}
	
	
}
