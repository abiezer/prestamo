package modelo.DAO;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Comuna;

public class ComunaDAO extends DB implements DAO<Comuna>{
	
	private ArrayList<Comuna> resultado;

	@Override
	public int create(Comuna objeto) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(int id, String campo, String valor) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<Comuna> get(int catidad) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public ArrayList<Comuna> getFromRegion(int region){
		ArrayList<Comuna> resultado = null;
		String consulta = "SELECT * FROM Region WHERE region_id = ?";
		
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(consulta);
			ps.setInt(1, region);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Comuna comuna = new Comuna();
				comuna.setId(rs.getInt("id"));
				comuna.setNombre(rs.getString("nombre"));
				comuna.setRegionId(rs.getInt("region_id"));
				resultado.add(comuna);
			}
			return resultado;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}

}
