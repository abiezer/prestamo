package modelo.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import modelo.Region;

public class RegionDAO extends DB implements DAO<Region>{
	
	@Override
	public int create(Region region) {
		int resultado = 0;
		Connection con = this.getConnection();
		
		String consulta = "INSERT INTO Region VALUES(?)";
		
		if(!(con ==null)) {
			try {
				PreparedStatement stmt = con.prepareStatement(consulta);
				stmt.setString(1, ((modelo.Region) region).getNombre());
				resultado = stmt.executeUpdate();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		return resultado;
	}
	
	@Override
	public int update(int id, String campo, String valor) {
		// TODO Auto-generated method stub
		Connection con = this.getConnection();
		int resultado = 0;
		String consulta = "UPDATE Region "
				+ "SET "+campo+" = ?"
				+ "WHERE id=?";
		try {
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setString(1, valor);
			ps.setInt(2, id);
			resultado = ps.executeUpdate();	
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
	}

	@Override
	public int delete(int id) {
		int resultado = 0;
		// TODO Auto-generated method stub
		Connection con = this.getConnection();
		String consulta = "DELETE Region WHERE id=?";
		try {
			PreparedStatement ps = con.prepareStatement(consulta);
			ps.setInt(1, id);
			resultado = ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resultado;
		
	}

	@Override
	public ArrayList<Region> get(int cantidad) {
		// TODO Auto-generated method stub
		String consulta = "SELECT TOP "+cantidad+" id, nombre FROM Region;";
		ArrayList<Region> regiones = new ArrayList<>();
		try {
			PreparedStatement ps = this.getConnection().prepareStatement(consulta);
			ResultSet rs = ps.executeQuery();
			
			while(rs.next()) {
				Region region = new Region();
				region.setId(rs.getInt("id"));
				region.setNombre(rs.getString("nombre"));
				regiones.add(region);
			}
			return regiones;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return regiones;
	}
	
	public ArrayList<Region> getSome(String id){
		
		return null;
	}

}
