package modelo.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import modelo.Usuario;

public class UsuarioDAO extends DB implements DAO<Usuario>{
	
	public Usuario auntenticate(String correo, String clave) {
		String consulta = "SELECT * FROM Usuario WHERE correo = ? AND clave=?";
		Usuario unUsuario = null;
		Connection con =  this.getConnection();
		try {
			PreparedStatement unStmt = con.prepareStatement(consulta);
			unStmt.setString(1, correo);
			unStmt.setString(2, clave);
			ResultSet rs = unStmt.executeQuery();
			while(rs.next()) {
				unUsuario = new Usuario();
				unUsuario.setId(rs.getInt("id"));
				unUsuario.setRut(rs.getString("rut"));
				unUsuario.setNombre(rs.getString("nombre"));
				unUsuario.setApellido(rs.getString("apellido"));
				unUsuario.setCorreo(rs.getString("correo"));
				unUsuario.setTipo(rs.getString("tipo"));
				unUsuario.setCalle(rs.getString("calle"));
				unUsuario.setNumeroCasa(rs.getString("numerocasa"));
				unUsuario.setReputacionId(rs.getInt("reputacion_id"));
				unUsuario.setComunaId(rs.getInt("comuna_id"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return unUsuario;
	}

	public int create(Usuario usuario) {
		// TODO Auto-generated method stub
		int resultado = 0;
		String Consulta = "INSERT INTO Usuario"
				+ "(rut,"
				+ "nombre,"
				+ "apellido,"
				+ "correo,"
				+ "tipo,"
				+ "calle,"
				+ "numerocasa,"
				+ "reputacion_id,"
				+ "comuna_id)"
				+ "VALUES(?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement stmt = this.getConnection().prepareStatement(Consulta);
			stmt.setString(1, usuario.getRut());
			stmt.setString(2, usuario.getNombre());
			stmt.setString(3, usuario.getApellido());
			stmt.setString(4, usuario.getCorreo());
			stmt.setString(5, usuario.getTipo());
			stmt.setString(6, usuario.getCalle());
			stmt.setString(7, usuario.getNumeroCasa());
			stmt.setInt(8, usuario.getReputacionId());
			stmt.setInt(9, usuario.getComunaId());
			resultado = stmt.executeUpdate();
			return resultado;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage().toString());
		}
		return resultado;
	}
	
	@Override
	public int update(int id,String campo,String valor) {
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ArrayList<Usuario> get(int cantidad) {
		Usuario usuario= null;
		ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
		String consulta = "SELECT TOP "+cantidad
				+ " id, "
				+ "rut, "
				+ "nombre, "
				+ "apellido, "
				+ "correo, "
				+ "tipo, "
				+ "calle, "
				+ "numerocasa, "
				+ "reputacion_id, "
				+ "comuna_id "
				+ "FROM Usuario"; 
		try {
			Connection con =  this.getConnection();
			PreparedStatement unStmt = con.prepareStatement(consulta);
			ResultSet rs = unStmt.executeQuery();
			
			while(rs.next()) {
				usuario = new Usuario();
				usuario.setId(rs.getInt("id"));
				usuario.setRut(rs.getString("rut"));
				usuario.setNombre(rs.getString("nombre"));
				usuario.setApellido(rs.getString("apellido"));
				usuario.setCorreo(rs.getString("correo"));
				usuario.setTipo(rs.getString("tipo"));
				usuario.setCalle(rs.getString("calle"));
				usuario.setNumeroCasa(rs.getString("numerocasa"));
				usuario.setReputacionId(rs.getInt("reputacion_id"));
				usuario.setComunaId(rs.getInt("comuna_id"));
				usuarios.add(usuario);
			}
			return usuarios;
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage().toString());
		}
		return usuarios;
	}

}
