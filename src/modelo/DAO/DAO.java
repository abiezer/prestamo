package modelo.DAO;

import java.util.ArrayList;

public interface DAO<T>{
	int create(T objeto);
	public abstract int update(int id,String campo,String valor);
	public abstract int delete(int id);
	public abstract ArrayList<T> get(int catidad);	
}
