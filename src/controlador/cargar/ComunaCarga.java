package controlador.cargar;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import modelo.Comuna;
import modelo.DAO.ComunaDAO;

/**
 * Servlet implementation class ComunaCarga
 */
@WebServlet("/ComunaCarga")
public class ComunaCarga extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ComunaCarga() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		int region_id = Integer.parseInt(request.getParameter("region_id"));
		
		ComunaDAO unaComunaDAO = new ComunaDAO();
		ArrayList<Comuna> unasComunas = unaComunaDAO.getFromRegion(region_id);
		
		String select = "<select name='comuna_id'>";
		
		if(!(unasComunas.equals(null))) {
			for(Comuna unaComuna: unasComunas) {
				select += "<option value='"+ unaComuna.getId() +"'>"+ unaComuna.getNombre() +"</option>";
			}
		}
		
		select+= "</select>";
		
		
		response.getWriter().append(select);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
